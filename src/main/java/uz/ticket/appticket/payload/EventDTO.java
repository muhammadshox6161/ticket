package uz.ticket.appticket.payload;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class EventDTO {
    private String name;
    private String description;
    private Timestamp time;
    private Integer hallId;
    private Integer maxTicketAmount;
    private Double price;
}
