package uz.ticket.appticket.payload;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class TicketDTO {
    private Integer eventId;
    private String guestName;
    private Timestamp createdAt;
}
