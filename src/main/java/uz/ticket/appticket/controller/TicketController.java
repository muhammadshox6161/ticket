package uz.ticket.appticket.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.appticket.model.Ticket;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.payload.TicketDTO;

@RequestMapping("/api/ticked")
public interface TicketController {
    @PostMapping
    ResultMessage addTicked(@RequestBody TicketDTO ticketDTO);

    @GetMapping("/{id}")
    Ticket getTicked(@PathVariable Integer id);

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    ResultMessage updateTicked(@PathVariable Integer id,@RequestBody TicketDTO ticketDTO);

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    ResultMessage deleteTicked(@PathVariable Integer id);
}
