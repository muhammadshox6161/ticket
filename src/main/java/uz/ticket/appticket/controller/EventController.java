package uz.ticket.appticket.controller;

import org.springframework.web.bind.annotation.*;
import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.payload.EventDTO;
import uz.ticket.appticket.payload.ResultMessage;

@RequestMapping("/api/event")
public interface EventController {
    @PostMapping
    ResultMessage addEvent(@RequestBody EventDTO eventDTO);

    @GetMapping("/{id}")
    Event getEvent(@PathVariable Integer id);

    @RequestMapping(value = "/{id}",method = RequestMethod.PUT)
    ResultMessage updateEvent(@PathVariable Integer id,@RequestBody EventDTO eventDTO);

    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    ResultMessage deleteEvent(@PathVariable Integer id);
}
