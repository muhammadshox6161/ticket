package uz.ticket.appticket.controller.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.appticket.controller.EventController;
import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.payload.EventDTO;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.service.EventService;

@RestController
@RequiredArgsConstructor
public class EventControllerImpl implements EventController {
    final EventService eventService;

    @Override
    public ResultMessage addEvent(EventDTO eventDTO) {
       return eventService.addEvent(eventDTO);
    }

    @Override
    public Event getEvent(Integer id) {
        return eventService.getOneEvent(id);
    }

    @Override
    public ResultMessage updateEvent(Integer id, EventDTO eventDTO) {
        return eventService.updateEvent(id,eventDTO);
    }

    @Override
    public ResultMessage deleteEvent(Integer id) {
        return eventService.deleteEvent(id);
    }
}
