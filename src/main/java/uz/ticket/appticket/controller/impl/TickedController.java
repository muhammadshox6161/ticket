package uz.ticket.appticket.controller.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import uz.ticket.appticket.controller.TicketController;
import uz.ticket.appticket.model.Ticket;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.payload.TicketDTO;
import uz.ticket.appticket.service.TickedService;

@RestController
@RequiredArgsConstructor
public class TickedController implements TicketController {

    private final TickedService tickedService;

    @Override
    public ResultMessage addTicked(TicketDTO ticketDTO) {
       return tickedService.addTicked(ticketDTO);
    }

    @Override
    public Ticket getTicked(Integer id) {
        return tickedService.getTicked(id);
    }

    @Override
    public ResultMessage updateTicked(Integer id, TicketDTO ticketDTO) {
        return tickedService.updateTicked(id,ticketDTO);
    }

    @Override
    public ResultMessage deleteTicked(Integer id) {
        return tickedService.deletedTicked(id);
    }
}
