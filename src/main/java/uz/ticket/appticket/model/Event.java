package uz.ticket.appticket.model;

import jakarta.persistence.*;
import lombok.*;

import java.sql.Timestamp;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Event {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;
    private String description ;
    private Timestamp time;
    @ManyToOne
    private Hall hall;
    private Integer maxTicketAmount;
    private Double price;
}
