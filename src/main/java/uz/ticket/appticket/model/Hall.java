package uz.ticket.appticket.model;

import jakarta.persistence.*;
import lombok.Data;

@Data
@Entity
public class Hall {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(nullable = false)
    private String name;
    private String location;
    private Integer capacity;

}
