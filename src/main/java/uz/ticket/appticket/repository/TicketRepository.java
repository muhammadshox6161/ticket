package uz.ticket.appticket.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.ticket.appticket.model.Ticket;

public interface TicketRepository extends JpaRepository<Ticket,Integer> {
}
