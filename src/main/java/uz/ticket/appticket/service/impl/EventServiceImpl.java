package uz.ticket.appticket.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.model.Hall;
import uz.ticket.appticket.payload.EventDTO;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.repository.EventRepository;
import uz.ticket.appticket.repository.HallRepository;
import uz.ticket.appticket.service.EventService;

import java.sql.Timestamp;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class EventServiceImpl implements EventService {

   private final EventRepository eventRepository;
   private final HallRepository hallRepository;
    @Override
    public ResultMessage addEvent(EventDTO eventDTO) {
        Event event = eventMapper(new Event(),eventDTO);
        eventRepository.save(event);
        return new ResultMessage(true,"Saved");
    }

    @Override
    public Event getOneEvent(Integer id) {
        Optional<Event> optionalEvent = eventRepository.findById(id);
        return optionalEvent.get();
    }

    @Override
    public ResultMessage updateEvent(Integer id, EventDTO eventDTO) {
        Optional<Event> optionalEvent = eventRepository.findById(id);
        if (optionalEvent.isPresent()){
            Event event = optionalEvent.get();
            eventMapper(event,eventDTO);
            eventRepository.save(event);
            return new ResultMessage(true,"Update");
        }
        return new ResultMessage(false,"event not found with id = "+id);
    }

    @Override
    public ResultMessage deleteEvent(Integer id) {
        if (!eventRepository.existsById(id)){
            return new ResultMessage(false,"event not found with id = "+id);
        }
        eventRepository.deleteById(id);
        return new ResultMessage(true,"successfully deleted");
    }

    private Event eventMapper(Event event,EventDTO eventDTO){
        Optional<Hall> optionalHall = hallRepository.findById(eventDTO.getHallId());
        if (optionalHall.isPresent()){
            event.setName(eventDTO.getName());
            event.setDescription(eventDTO.getDescription());
            event.setTime(eventDTO.getTime());
            event.setHall(optionalHall.get());
            event.setMaxTicketAmount(eventDTO.getMaxTicketAmount());
            event.setPrice(eventDTO.getPrice());
            return event;
        }
        return new Event();
    }
}
