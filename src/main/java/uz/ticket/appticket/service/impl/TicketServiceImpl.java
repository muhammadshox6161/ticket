package uz.ticket.appticket.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.model.Ticket;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.payload.TicketDTO;
import uz.ticket.appticket.repository.EventRepository;
import uz.ticket.appticket.repository.TicketRepository;
import uz.ticket.appticket.service.TickedService;

import java.sql.Timestamp;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TicketServiceImpl implements TickedService {
   private final TicketRepository ticketRepository;
   private final EventRepository eventRepository;

    @Override
    public ResultMessage addTicked(TicketDTO ticketDTO) {
        Ticket ticket = ticketMapper(new Ticket(),ticketDTO);
        ticketRepository.save(ticket);
        return new ResultMessage(true,"Saved");
    }

    @Override
    public Ticket getTicked(Integer id) {
        Optional<Ticket> ticketOptional = ticketRepository.findById(id);
        return ticketOptional.orElseGet(null);
    }

    @SneakyThrows
    @Override
    public ResultMessage updateTicked(Integer id, TicketDTO ticketDTO) {
        Optional<Ticket> optionalTicket = ticketRepository.findById(id);
        if (optionalTicket.isPresent()){
            Ticket ticket = optionalTicket.get();
            ticketMapper(ticket,ticketDTO);
            ticketRepository.save(ticket);
            return new ResultMessage(true,"Update");
        }
        return new ResultMessage(false,"ticked not found with id = "+id);
    }

    @Override
    public ResultMessage deletedTicked(Integer id) {
        if (!ticketRepository.existsById(id)){
            return new ResultMessage(false,"ticked not found with id = "+id);
        }
        ticketRepository.deleteById(id);
        return new ResultMessage(true,"successfully deleted");
    }
    private Ticket ticketMapper(Ticket ticket,TicketDTO ticketDTO){
        Optional<Event> optionalEvent = eventRepository.findById(ticketDTO.getEventId());
        if (optionalEvent.isPresent()){
            ticket.setEvent(optionalEvent.get());
            ticket.setGuestName(ticketDTO.getGuestName());
            ticket.setCreatedAt(ticketDTO.getCreatedAt());
            return ticket;
        }
        return new Ticket();
    }
}
