package uz.ticket.appticket.service;

import uz.ticket.appticket.model.Ticket;
import uz.ticket.appticket.payload.ResultMessage;
import uz.ticket.appticket.payload.TicketDTO;

public interface TickedService {

    ResultMessage addTicked(TicketDTO ticketDTO);

    Ticket getTicked(Integer id);

    ResultMessage updateTicked(Integer id, TicketDTO ticketDTO);

    ResultMessage deletedTicked(Integer id);
}
