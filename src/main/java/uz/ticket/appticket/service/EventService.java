package uz.ticket.appticket.service;

import uz.ticket.appticket.model.Event;
import uz.ticket.appticket.payload.EventDTO;
import uz.ticket.appticket.payload.ResultMessage;

public interface EventService {

    ResultMessage addEvent(EventDTO eventDTO);

    Event getOneEvent(Integer id);

    ResultMessage updateEvent(Integer id, EventDTO eventDTO);

    ResultMessage deleteEvent(Integer id);
}
